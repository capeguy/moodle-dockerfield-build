FROM php:apache

LABEL maintainer="kenneth@itachi1706.com"

RUN apt-get update && apt-get install -y gnupg git libzip-dev libpng-dev libjpeg-dev libicu-dev libxml2-dev libpq-dev zip locales unixodbc-dev
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list && apt-get update
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql17 mssql-tools && rm -rf /var/lib/apt/lists*
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
RUN docker-php-ext-install mysqli pdo_mysql pdo_pgsql pgsql zip gd intl xmlrpc soap opcache
RUN pecl install sqlsrv pdo_sqlsrv xdebug
RUN docker-php-ext-enable sqlsrv pdo_sqlsrv xdebug
RUN mkdir -p /opt/moodle && ln -s /opt/moodle /var/www/html && chown -R www-data:www-data /var/www/html
RUN mkdir -p /data/moodle/dir && chown -R www-data:www-data /data/moodle/dir
RUN mkdir -p /data/moodle/punit && chown -R www-data:www-data /data/moodle/dir
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && php composer-setup.php && php -r "unlink('composer-setup.php');"
RUN echo 'memory_limit = 1G' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini;

# Set the locale
RUN sed -i -e 's/# en_AU.UTF-8 UTF-8/en_AU.UTF-8 UTF-8/' /etc/locale.gen && locale-gen
ENV LANG en_AU.UTF-8  
ENV LANGUAGE en_AU:en  
ENV LC_ALL en_AU.UTF-8 
RUN locale-gen en_AU.UTF-8

ADD info.php /var/www/html
WORKDIR /var/www/html
#RUN service apache2 start
